#!/bin/bash

sudo /usr/bin/apt-get update
sudo /usr/bin/apt-get upgrade -y

sudo /usr/bin/apt-get install -y joe git locate python3-dev python-dev python3-numpy mercurial libsdl-dev libsdl-image1.2-dev libsdl-mixer1.2-dev libsdl-ttf2.0-dev libsmpeg-dev libportmidi-dev libavformat-dev libswscale-dev python3-pip python3-pifacedigitalio

mkdir ~/.ssh
chmod 700 ~/.ssh
echo "|1|14cEp6UJpIjkIj3AXZAtsTRhm0E=|OSDLieIqyvBUVEMDclo1R4/QMMY= ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAubiN81eDcafrgMeLzaFPsw2kNvEcqTKl/VqLat/MaB33pZy0y3rJZtnqwR2qOOvbwKZYKiEO1O6VqNEBxKvJJelCq0dTXWT5pbO2gDXC6h6QDXCaHo6pOHGPUy+YBaGQRGuSusMEASYiWunYN0vCAI8QaXnWMXNMdFP3jHAJH0eDsoiGnLPBlBp4TNm6rYI74nMzgz3B9IikW4WVK+dc8KZJZWYjAuORU3jc1c/NPskD2ASinf8v3xnfXeukU0sJ5N6m5E8VLjObPEO+mN2t/FZTMZLiFqPWc/ALSqnMnnhwrNi2rbfg/rd/IpL8Le3pSBne8+seeFVBoGqzHM9yXw==" >~/.ssh/known_hosts
echo "|1|f6yF6tJSxGUIs9oi/scfRW7jK/E=|irTiHAqcVX2qDFq0QDnvGWG9KAg= ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAubiN81eDcafrgMeLzaFPsw2kNvEcqTKl/VqLat/MaB33pZy0y3rJZtnqwR2qOOvbwKZYKiEO1O6VqNEBxKvJJelCq0dTXWT5pbO2gDXC6h6QDXCaHo6pOHGPUy+YBaGQRGuSusMEASYiWunYN0vCAI8QaXnWMXNMdFP3jHAJH0eDsoiGnLPBlBp4TNm6rYI74nMzgz3B9IikW4WVK+dc8KZJZWYjAuORU3jc1c/NPskD2ASinf8v3xnfXeukU0sJ5N6m5E8VLjObPEO+mN2t/FZTMZLiFqPWc/ALSqnMnnhwrNi2rbfg/rd/IpL8Le3pSBne8+seeFVBoGqzHM9yXw==" >>~/.ssh/known_hosts

echo "Host	remote bitbucket.org" >~/.ssh/config
echo "Hostname	bitbucket.org" >>~/.ssh/config
echo "IdentityFile	~/.ssh/raspberry_autoconfig_id" >>~/.ssh/config



pushd ~/
git clone https://github.com/HakanL/animatroller.git
popd

pushd ~/
git clone https://hakanl@bitbucket.org/hakanl/raspberryautoconfig.git
popd

pushd ~/
hg clone https://bitbucket.org/pygame/pygame
cd pygame
/usr/bin/python3 setup.py build
sudo /usr/bin/python3 setup.py install
popd

/usr/bin/pip-3.2 install python-osc
/usr/bin/pip-3.2 install pyserial
