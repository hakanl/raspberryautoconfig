#!/bin/bash

cd /root

pushd /root/animatroller
git pull
popd

pushd /root/raspberryautoconfig
git pull
popd

pushd /root/
#git clone git@bitbucket.org:hakanl/halloweensounds.git
#cd halloweensounds
git clone git@bitbucket.org:hakanl/christmassounds.git
cd christmassounds
git pull
popd


/usr/bin/amixer sset 'PCM',0 100% 100%

cd /root


mac=$(/sbin/ifconfig eth0 | grep -o -E '([[:xdigit:]]{1,2}:){5}[[:xdigit:]]{1,2}')
filename=raspberryautoconfig/config/${mac//:/-}.sh

$filename
