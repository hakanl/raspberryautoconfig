# Raspberry Pi auto config

1. Boot up Raspbian through NOOB
2. Transfer/download first.sh (https://bitbucket.org/hakanl/raspberryautoconfig/raw/master/first.sh)
3. Comment out blacklist of SPI module (/etc/modprobe.d/raspi-blacklist.conf)
4. Transfer raspberry_autoconfig_id to /root/.ssh/raspberry_autoconfig_id
5. Add call to /root/raspberryautoconfig/boot.sh & in /etc/rc.local
